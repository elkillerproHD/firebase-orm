"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validateModel = validateModel;

var _bycontract = require("bycontract");

function validateModel(data, values) {
  (0, _bycontract.validate)(data, "array");
  let toValidateData = {};
  data.forEach(dta => {
    if (dta.required) {
      toValidateData[dta.name] = dta.type;
    }
  });
  (0, _bycontract.validate)(values, { ...toValidateData
  });
}
