"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = UploadLocalFileToFirebase;

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _storage = require("@google-cloud/storage");

var _envVariables = _interopRequireDefault(require("../env.variables.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// const imagePath = Path.join(__dirname, '../migrations/seeds/foodCategory/images/arroz.jpg');
const storagePath = _path.default.join(process.cwd(), '/storage');

async function UploadLocalFileToFirebase(data = {}) {
  const {
    path = {},
    name = '',
    fileType = '',
    firebaseConstantsPath: firebaseConstantsPath,
    firebaseConstantsFile: firebaseConstantsFile,
    handleError = () => {},
    isPublic = false
  } = data;

  try {
    const file = _fs.default.readFileSync(path);

    const nodeEnv = process.env.NODE_ENV || "production";

    const FirebaseConstants = require(firebaseConstantsPath);

    const storageRef = new _storage.Storage({
      projectId: FirebaseConstants.project_id,
      keyFilename: firebaseConstantsFile
    });
    console.log(file);
    const bucket = storageRef.bucket(`${FirebaseConstants.project_id}.appspot.com`);
    const gcsname = Date.now() + name;
    const fileBucket = bucket.file(gcsname);
    const stream = fileBucket.createWriteStream({
      metadata: {
        contentType: fileType
      },
      resumable: false
    });
    stream.on('error', err => {
      console.log("err");
      console.log(err);
    });
    stream.on('finish', () => {
      console.log("Finish");
      console.log(gcsname);
      console.log(fileBucket);
    });
    stream.end(file);

    if (isPublic) {
      await fileBucket.makePublic();
    }

    return Promise.resolve(gcsname);
  } catch (err) {
    handleError(err);
    console.log(err);
  }
}
