"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
const RunServer = require(`${__dirname}/server`)
exports.Server = RunServer;
exports.setNewImage = setNewImage;
exports.CheckAndUploadFiles = CheckAndUploadFiles;
exports.MultipleWriteJson = MultipleWriteJson;
exports.CheckPath = CheckPath;
exports.default = exports.imgType = void 0;

var _firebase = _interopRequireDefault(require("firebase"));

var _validateModel = require("./functions/validateModel");

var _bycontract = require("bycontract");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const uploadFileFirebasePath = "./functions/uploadLocalFileToFirebase";
const uploadFileLocalPath = "./functions/uploadLocalFileToLocal"; // import {UploadLocalFileToFirebase, UploadLocalFileToLocal} from "../functions/uploadLocalFileToFirebase";
// import {collection} from "./foodCategory";

const imgType = {
  name: "string",
  path: "string",
  fileType: "string",
  file: "boolean"
};
exports.imgType = imgType;
(0, _bycontract.typedef)("img", imgType);

function setNewImage({
                       data
                     }) {
  const {
    name = "",
    local = false
  } = data;
  return {
    name,
    local
  };
}
/*
*  JSON de ejemplo para realizar multiple writes
* [{
* // CHECK es el modelo a utilizar en la vadilacion con bycontract
*   check: foodModel
*   model: "FOOD-CATEGORY",
*   action: "add",
* // DATA tiene que ser el modelo normal con los datos
*   data: [
*     name: "Pastas",
*     img: {
*       name: "pastas.png",
*       path: "asd/pastas.png",
*       fileType: "image/png"
*     }
*   ]
* }]
* */


const errorCondition = "The Condition is not valid. The valid contions are == != < <= > >= and your condition is ";
const errorInSearchParameters = "You need to pass valid parameters ({type, value})";
const errorID = "You must provide an ID for search";
const errorIDMany2Many = "You must provide an ID in the constructor to7 work with many2many";
const errorMany2ManyFields = "You must provide collection, name and table name in the model of Many2Many";
const preModels = {
  name: "id",
  type: "string",
  required: true
};
const preModelsMany2Many = [{
  name: "name",
  type: "string",
  required: true
}, {
  name: "collection",
  type: "string",
  required: true
}, {
  name: "tableName",
  type: "string",
  required: true
}];

async function CheckAndUploadFiles(data) {
  // This Function get by params the validated data for upload to firestore.
  // The next last step before upload it is, check if some property is a file
  // For every file in the data, we going to upload it to Firebase if we are in production
  // or in the /<rootOfYourFolder>/storage if we are in development
  // and property of fyle by property of file also change de value of the property for
  // the url of the uploaded file
  const keys = Object.keys(data);
  const values = Object.values(data);

  for (let i = 0; i < values.length; i++) {
    const value = values[i];

    if (value.file) {
      const nodeEnv = process.env.NODE_ENV || "production";

      if (nodeEnv === "development") {
        const UploadLocalFileToLocal = await Promise.resolve(`${uploadFileLocalPath}`).then(s => _interopRequireWildcard(require(s)));
        const name = await UploadLocalFileToLocal.default(value);
        data[keys[i]] = {
          local: true,
          name
        };
      } else if (nodeEnv === "production") {
        const UploadLocalFileToFirebase = await Promise.resolve(`${uploadFileFirebasePath}`).then(s => _interopRequireWildcard(require(s)));
        const name = await UploadLocalFileToFirebase.default({ ...value,
          isPublic: true
        });
        data[keys[i]] = {
          local: false,
          name
        };
      }
    }
  }

  delete data.file;
  return Promise.resolve(data);
}

class FireModel {
  constructor(props) {
    _defineProperty(this, "create", async (dta = {}) => {
      const {
        success = function () {},
        fail = function () {}
      } = dta;
      const {
        modelRef,
        model
      } = this;
      let {
        data
      } = this;
      (0, _validateModel.validateModel)(model, data);
      data = await CheckAndUploadFiles(data);
      await modelRef.add(data).then(doc => {
        success(doc);
        return Promise.resolve(doc);
      }).catch(function (error) {
        fail(error);
        return Promise.reject(error);
      });
    });

    _defineProperty(this, "all", async (dta = {}) => {
      const {
        success = function () {},
        fail = function () {}
      } = dta;
      const {
        modelRef
      } = this;
      let query = modelRef.get();
      await query.then(doc => {
        if (!doc.empty) {
          const values = doc.docs.map(dc => ({
            id: dc.id,
            ...dc.data()
          }));
          this.values = values;
          success(this.values);
          return Promise.resolve(values);
        } else {
          fail();
          return Promise.resolve();
        }
      }).catch(function (error) {
        console.log("Error getting document:", error);
        fail(error);
        return Promise.reject(error);
      });
    });

    _defineProperty(this, "set", async (dta = {}) => {
      const {
        success = function () {},
        fail = function () {}
      } = dta;
      const {
        modelRef,
        model,
        data
      } = this;
      (0, _validateModel.validateModel)(model.push(preModels), data);
      let noIdData = data;
      delete noIdData.id;
      noIdData = await CheckAndUploadFiles(noIdData);
      await modelRef.doc(data.id).set(noIdData).then(doc => {
        success(doc);
        return Promise.resolve(doc);
      }).catch(function (error) {
        fail(error);
        return Promise.reject(error);
      });
    });

    _defineProperty(this, "update", async (dta = {}) => {
      const {
        success = function () {},
        fail = function () {}
      } = dta;
      const {
        modelRef,
        data,
        model
      } = this;
      (0, _validateModel.validateModel)(model.push(preModels), data);
      let noIdData = data;
      delete noIdData.id;
      noIdData = await CheckAndUploadFiles(noIdData);
      await modelRef.doc(data.id).update(noIdData).then(doc => {
        success(doc);
        return Promise.resolve(doc);
      }).catch(function (error) {
        fail(error);
        return Promise.reject(error);
      });
    });

    _defineProperty(this, "delete", async (dta = {}) => {
      const {
        success = function () {},
        fail = function () {}
      } = dta;
      const {
        modelRef,
        db,
        data
      } = this;
      (0, _validateModel.validateModel)(preModels, data);
      const deleteRef = db.collection(`${this.collection}_deleted`);
      const noIdData = data;
      delete noIdData.id;
      await deleteRef.doc(data.id).set(noIdData).catch(function (error) {
        fail(error);
        return Promise.reject(error);
      });
      await modelRef.doc(data.id).delete().catch(function (error) {
        fail(error);
        return Promise.reject(error);
      });
      return Promise.resolve();
    });

    _defineProperty(this, "findById", async dta => {
      const {
        id = false,
        success = function () {},
        fail = function () {}
      } = dta;
      const {
        modelRef
      } = this;

      if (!id) {
        fail(errorID);
        console.error(errorID);
        return;
      }

      console.log(id);
      await modelRef.doc(id).get().then(doc => {
        console.log(doc);
        const queryData = {
          id: doc.id,
          ...doc.data()
        };
        success(queryData);
        this.values = queryData;
        return Promise.resolve(doc);

        if (doc.exists) {
          const queryData = {
            id: doc.id,
            ...doc.data()
          };
          success(queryData);
          this.values = queryData;
          return Promise.resolve(doc);
        } else {
          fail();
          return Promise.resolve();
        }
      }).catch(function (error) {
        console.log("Error getting document:", error);
        fail(error);
        return Promise.reject(error);
      });
    });

    _defineProperty(this, "findByValues", async dta => {
      const {
        type = false,
        value = false,
        condition = undefined,
        orderBy = false,
        limit = false,
        success = function () {},
        fail = function () {}
      } = dta;
      const {
        modelRef
      } = this;

      if (!type || !value) {
        fail(errorInSearchParameters);
        console.error(errorInSearchParameters);
        return;
      }

      if (condition !== false && condition !== "==" && condition !== "<" && condition !== ">" && condition !== ">=" && condition !== "<=" && condition !== "array-contains") {
        fail(`${errorCondition}${condition}`);
        console.error(errorCondition, condition);
        return;
      }

      let query = modelRef;

      if (condition) {
        query = query.where(type, condition, value);
      }

      if (orderBy) {
        query = query.orderBy(orderBy);
      }

      if (limit) {
        query = query.limit(limit);
      }

      await query.get().then(doc => {
        if (!doc.empty) {
          this.values = doc.docs.map(doc => ({
            id: doc.id,
            ...doc.data()
          }));
          success(doc);
          return Promise.resolve(this.values);
        } else {
          fail();
          return Promise.resolve();
        }
      }).catch(function (error) {
        console.log("Error getting document:", error);
        fail(error);
        return Promise.reject(error);
      }); // this.keys=query.keys();
      // query.keys().forEach((opt) => {
      //   this[opt] = query[opt]
      // })
      // console.log(this)
    });

    _defineProperty(this, "multipleWrites", async dta => {
      const {
        success = function () {},
        fail = function () {},
        data = []
      } = dta;
      const {
        modelRef,
        db,
        model
      } = this;
      const toMap = [];
      let objects = [];
      dta.map(query => {
        if (objects.length < 11) {
          objects.push(query);
        } else {
          toMap.push(objects);
          objects = [];
        }
      });

      if (objects.length > 0) {
        toMap.push(objects);
      }

      for (const dta of toMap) {
        const batch = db.batch();

        for (let query in dta) {
          let noIdData = query;

          switch (query.type) {
            case "add":
              (0, _validateModel.validateModel)(model, query.data);
              const docRef = modelRef.doc();
              query.data = await CheckAndUploadFiles(query.data);
              batch.set(docRef, query.data);
              break;

            case "set":
              (0, _validateModel.validateModel)(model.push(preModels), data);
              delete noIdData.id;
              noIdData = await CheckAndUploadFiles(noIdData);
              batch.set(modelRef.doc(query.data.id), noIdData);
              break;

            case "delete":
              const deleteRef = db.collection(`${this.collection}_deleted`);
              delete noIdData.id;
              batch.set(deleteRef.doc(query.data.id), noIdData);
              batch.delete(modelRef.doc(query.data.id));
              break;

            case "update":
              (0, _validateModel.validateModel)(model.push(preModels), data);
              delete noIdData.id;
              noIdData = await CheckAndUploadFiles(noIdData);
              batch.update(modelRef.doc(query.data.id), noIdData);
              break;

            default:
          }
        }

        batch.commit();
      }
    });

    this.db = _firebase.default.firestore();
    this.data = props.data;
    this.collection = props.collection;

    if (window.location.hostname === "localhost") {
      console.log("localhost detected!");
      this.db.settings({
        host: "localhost:8080",
        ssl: false
      });
    }

    this.model = props.model;
    this.modelRef = this.db.collection(this.collection);
  }

}

exports.default = FireModel;

function CheckPath(){
  const path = require("path")
  console.log(path.dirname(path.resolve()))
}

function MultipleWriteJson(props) {
  const {
    data,
    isFront
  } = props;

  const db = _firebase.default.firestore();

  const dev = true;

  if (dev) {
    console.log("localhost detected!");
    db.settings({
      host: "localhost:8080",
      ssl: false
    });
  } // Primero agrupamos las mismas colecciones


  const sameCollections = {};
  data.forEach(dta => {
    if (sameCollections[dta.model]) {
      sameCollections[dta.model].push(dta);
    } else {
      sameCollections[dta.model] = [];
      sameCollections[dta.model].push(dta);
    }
  });
  Object.values(sameCollections).forEach((dta, index) => {
    const toMap = [];
    let objects = [];
    const actualKey = Object.keys(sameCollections)[index];
    dta.map(query => {
      if (objects.length < 11) {
        objects.push(query);
      } else {
        toMap.push(objects);
        objects = [];
      }
    });

    if (objects.length > 0) {
      toMap.push(objects);
    }

    toMap.forEach(async dta => {
      const batch = db.batch();

      for (const query of dta) {
        let noIdData = query;

        switch (query.action) {
          case "add":
            (0, _validateModel.validateModel)(query.check, query.data);
            const docRef = db.collection(actualKey).doc();
            query.data = await CheckAndUploadFiles(query.data);
            batch.set(docRef, query.data);
            break;

          case "set":
            query.check.push(preModels);
            (0, _validateModel.validateModel)(query.check, query.data);
            delete noIdData.id;
            noIdData = await CheckAndUploadFiles(query.data);
            batch.set(db.collection(actualKey).doc(query.data.id), noIdData);
            break;

          case "delete":
            const deleteRef = db.collection(`${actualKey}_deleted`);
            delete noIdData.id;
            batch.set(deleteRef.doc(query.data.id), noIdData);
            batch.delete(db.collection(actualKey).doc(query.data.id));
            break;

          case "update":
            query.check.push(preModels);
            (0, _validateModel.validateModel)(query.check, query.data);
            delete noIdData.id;
            noIdData = await CheckAndUploadFiles(noIdData);
            batch.update(db.collection(actualKey).doc(query.data.id), noIdData);
            break;

          default:
        }
      }

      batch.commit().then(function () {
        console.log('Done.');
      }).catch(err => console.log(`There was an error: ${err}`));
    });
  });
}
