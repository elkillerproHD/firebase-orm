"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.storageUrl = void 0;
exports.default = RunServer;

const storageUrl = "http://localhost:3017/upload-storage";
exports.storageUrl = storageUrl;
"use strict";

const express = require('express');

const cors = require('cors');

const fileUpload = require('express-fileupload');

const path = require('path');

const fs = require('fs'); // const bodyParser = require('body-parser');
// const _ = require('lodash');

function RunServer(){
  const app = express(); //add other middleware

  app.use(cors()); // app.use(bodyParser.json());
  // app.use(bodyParser.urlencoded({extended: true}));

  app.use(fileUpload());
  app.use('/storage', express.static(path.resolve() + '/storage'));
  app.post('/upload-storage', (req, res) => {
    console.log("req.files");
    console.log(req.files);
    console.log(req.body);
    console.log(path.resolve());
    const name = req.body.name;
    const data = req.files.file.data;
    const storagePath = path.resolve() + "/storage";

    if (!fs.existsSync(storagePath)) {
      fs.mkdirSync(storagePath);
    }

    fs.writeFile(`${path.join(storagePath)}/${name}`, data, function (err, result) {
      if (err) console.log('error', err);
    });
  }); //start app

  const port = process.env.PORT || 3017;
  app.listen(port, () => console.log(`App is listening on port ${port}.`));
}
