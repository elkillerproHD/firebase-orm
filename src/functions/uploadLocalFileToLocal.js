"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = UploadLocalFileToLocal;

var _path = _interopRequireDefault(require("path"));

var _fs = _interopRequireDefault(require("fs"));

var _jimp = _interopRequireDefault(require("jimp"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const storagePath = _path.default.join(process.cwd(), '/storage');

async function UploadLocalFileToLocal(data = {}) {
  const {
    path = {},
    name = '',
    fileType = '',
    handleError = () => {},
    sizes = [],
    isImage = false
  } = data;
  const gcsname = Date.now() + name;

  if (!isImage) {
    try {
      const file = _fs.default.readFileSync(path);

      console.log(file);
      console.log(`${_path.default.join(storagePath)}/${gcsname}`);

      if (!_fs.default.existsSync(storagePath)) {
        _fs.default.mkdirSync(storagePath);
      }

      _fs.default.writeFile(`${_path.default.join(storagePath)}/${gcsname}`, file, function (err, result) {
        if (err) console.log('error', err);
      });

      return Promise.resolve(gcsname);
    } catch (err) {
      handleError(err);
      console.log(err);
    }
  } else {
    console.log(sizes);

    for (let i = 0; i < sizes.length; i++) {
      const size = sizes[i];
      console.log(size);
      const image = await _jimp.default.read(path);
      console.log("image");
      console.log(image);
      const sizegcsname = `${size}/${gcsname}`;
      await image.resize(size, _jimp.default.AUTO);
      await image.writeAsync(`${storagePath}/${sizegcsname}`);
    }

    try {
      const file = _fs.default.readFileSync(path);

      console.log(file);
      console.log(`${_path.default.join(storagePath)}/${gcsname}`); // if (!Fs.existsSync(storagePath)){
      //   Fs.mkdirSync(storagePath);
      // }
      // Fs.writeFile(`${Path.join(storagePath)}/${gcsname}`, file, function(err, result) {
      //   if(err) console.log('error', err);
      // });

      return Promise.resolve(gcsname);
    } catch (err) {
      handleError(err);
      console.log(err);
    }
  }
}
